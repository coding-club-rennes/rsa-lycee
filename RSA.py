#!/usr/bin/python3
#-*-coding:Utf-8 -*

import sys

def PGCD(a, b):
    if a == 0 or b == 0:
        raise ValueError("a is NULL or b is NULL")
    elif a < b:
        a,b = b,a
    r = a % b
    a = b
    b = r
    while r != 0:
        r = a % b
        a = b
        b = r
    return a

def calcKeys(p, q):
    n = p * q
    phi_n = (p - 1) * (q - 1)
    e = 0
    if (p > q):
        e = p + 1
    else:
        e = q + 1
    while (PGCD(e, phi_n) != 1):
        e = e + 1
    d = 0
    while (((e * d) % phi_n) != 1):
        d = d + 1
    return (e, d, n)

try:
    res = calcKeys(577, 857) # calcKeys(p, q)
    e = res[0]
    d = res[1]
    n = res[2]
    ### Your code goes here ###

    ###########################

except ValueError as e:
    print("Error: {}".format(e))
